import React from 'react'
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom'

import './App.css'
import ArticleSingle from './pages/article-single'

const App = () => {
  return (
    <div className='App'>
      <BrowserRouter>
        <Routes>
          {/*<Route path='/' element={<App />} />*/}
          <Route exact path='/content/article/can-3-years-old-kid-drink-boba-milk-tea/' element={<ArticleSingle />} />
          {/*<Route path='invoices' element={<Invoices />} />*/}
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
